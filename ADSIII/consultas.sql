
#Criar tabela
CREATE TABLE `veiculo` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`ano_modelo` INT(11) NOT NULL DEFAULT '0',
	`ano_fabricacao` INT(11) NOT NULL DEFAULT '0',
	`data_venda` DATE ,
	`placa` VARCHAR(8) NULL DEFAULT '0',
	`modelo` VARCHAR(50) NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)


# Comentário:Selecionar elementos

#Todos


#Todos com restricaoveiculo
select * from veiculo
where 
	ano_modelo = 2013
	and modelo = 'Siena'
	
select * from Veiculo
where modelo like("%a%")

#Inserir elementos
insert into veiculo(placa, modelo, ano_fabricacao, ano_modelo, data_venda) 
values("OEG-7117","Novo Palio", 2012, 2013, "2012-09-07")

insert into veiculo(placa, modelo, ano_fabricacao, ano_modelo, data_venda) 
values("OEG-7118","Siena", 2012, 2013, "2012-09-07")

insert into veiculo(placa, modelo, ano_fabricacao, ano_modelo, data_venda) 
values("LWM-9369","Corsa Hatch", 2003, 2004, "2009-02-28")

# Apagar registros
delete from veiculo

# resetar tabela
drop table veiculo


CREATE TABLE `veiculo2` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`ano_modelo` INT(11) NOT NULL DEFAULT '0',
	`ano_fabricacao` INT(11) NOT NULL DEFAULT '0',
	`data_venda` DATE NULL DEFAULT NULL,
	`placa` VARCHAR(8) NULL DEFAULT '0',
	`modelo` VARCHAR(50) NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=7;