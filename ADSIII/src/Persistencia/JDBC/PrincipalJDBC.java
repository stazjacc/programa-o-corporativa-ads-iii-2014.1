package Persistencia.JDBC;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import Persistencia.Entidade.Veiculo;

public class PrincipalJDBC {
	
	public static void main(String[] args) throws HeadlessException, ParseException {
		
		//Abrir Conex�o com Banco de Dados.
		Connection con = null;
		try {
			con = FabricaDeConexoes.getConnection();
			System.out.println("Conectado");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Persistir o Veiculo Criado.
		inserirJDBC(con);
		
		//Listar todos
		List<Veiculo> veiculos = BuscarListaJDBC(con);
		
		for (Veiculo v : veiculos) {
			System.out.println(v);
		}
		
		
		//Encerrar conex�o
		try {
			con.close();
			System.out.println("Conex�o Encerrada");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static List<Veiculo> BuscarListaJDBC(Connection con) {
		
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		
		String sql = "select * from veiculo";
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Veiculo veiculo = new Veiculo();
				veiculo.setId(rs.getInt("id"));
				veiculo.setPlaca(rs.getString("placa"));
				veiculo.setModelo(rs.getString("modelo"));
				veiculo.setAnoFabricacao(rs.getInt("ano_fabricacao"));
				veiculo.setAnoModelo(rs.getInt("ano_modelo"));
				
				Calendar data = Calendar.getInstance();
				data.setTime(rs.getDate("data_venda"));
				veiculo.setDataVenda(data);
				
				veiculos.add(veiculo);
			}
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return veiculos;
	}

	private static void inserirJDBC(Connection con) throws HeadlessException, ParseException {
		
		//Inserir dados no BD com preparedStatemante
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(JOptionPane.showInputDialog("Placa ex. ABC-1234"));
		veiculo.setModelo(JOptionPane.showInputDialog("Modelo"));
		veiculo.setAnoFabricacao(Integer.valueOf(JOptionPane.showInputDialog("Ano Fabrica��o")));
		veiculo.setAnoModelo(Integer.valueOf(JOptionPane.showInputDialog("Ano Modelo")));
		
		//Que sufoco pra trabalhar com datas... Salve Java 8
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date data = formatter.parse(JOptionPane.showInputDialog("Data (ex. 01/12/2011)"));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		veiculo.setDataVenda(calendar);
		
		String sql = "insert into veiculo (placa, modelo, ano_fabricacao, ano_modelo, data_venda) values (?,?, ?, ?, ?) ";
		
		try {
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, veiculo.getPlaca());
			ps.setString(2, veiculo.getModelo());
			ps.setInt(3, veiculo.getAnoFabricacao());
			ps.setInt(4, veiculo.getAnoModelo());
			ps.setDate(5, new java.sql.Date(veiculo.getDataVenda().getTimeInMillis()));
			
			ps.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
