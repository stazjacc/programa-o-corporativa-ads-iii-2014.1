package Persistencia.Entidade;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Veiculo {
	
	private int id;
	private String placa;
	private String modelo;
	private int anoModelo;
	private int anoFabricacao;
	private Calendar dataVenda;
	
	public Veiculo() {
		// TODO Auto-generated constructor stub
	}
	
	public Veiculo(String placa, String modelo, int anoModelo,
			int anoFabricacao, Calendar dataVenda) {
		this.placa = placa;
		this.modelo = modelo;
		this.anoModelo = anoModelo;
		this.anoFabricacao = anoFabricacao;
		this.dataVenda = dataVenda;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Veiculo: "+id+" - "+this.getModelo()+" - "+this.getPlaca()+" - "+this.getAnoFabricacao()+"/"+this.getAnoModelo() + " Venda " + this.getDataVendaddMMyyyy()  ;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getAnoModelo() {
		return anoModelo;
	}

	public void setAnoModelo(int anoModelo) {
		this.anoModelo = anoModelo;
	}

	public int getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(int anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}
	
	public String getDataVendaddMMyyyy(){
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(this.dataVenda.getTime());
		
	}

	public Calendar getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Calendar dataVenda) {
		this.dataVenda = dataVenda;
	}

}
